besssugo: a graphical visualization tool for wasora
===================================================

Besssugo is a plugin for [wasora](http:/www.talador.com.ar/jeremy/wasora) that provides means to perform graphical representations of wasora objects by using the [SDL library](https://www.libsdl.org/). By working on top of wasora, it uses an input file composed of keywords and arguments to provide means to

 * define a window size
 * draw graphic primitives (points, segments, rectangles & circles) whose location and colors are given by algebraic expressions (which may involve for example spline-interpolated pointwise-defined functions)
 * load image files whose location and angle are given by algebraic expressions
 * dump individual frames into file bitmaps which can be used to generate standalone videos

See the `INSTALL` file for installation instructions.  
See the `examples` subdirectory for examples of application.  


Licensing
---------

besssugo is distributed under the terms of the [GNU General Public License](http://www.gnu.org/copyleft/gpl.html) version 3 or (at your option) any later version. The source tree includes free snippets of code:

  * SDL_image: an example image loading library for use with SDL, copyright (C) 1997--2013 Sam Lantinga
  * SDL2_gfxPrimitives: graphics primitives for SDL, copyright (C) 2012--2014  Andreas Schiffler




Further information
-------------------

See the file `INSTALL` for compilation and installation instructions.  
See the directory `examples` for the test suite and other examples.  
See the contents of directory `doc` for full documentation.  

Home page: <http://www.talador.com.ar/jeremy/wasora/besssugo>  
Mailing list and bug reports: <wasora@talador.com.ar>  


besssugo is copyright (C) 2010--2015 jeremy theler  
besssugo is licensed under [GNU GPL version 3](http://www.gnu.org/copyleft/gpl.html) or (at your option) any later version.  
besssugo is free software: you are free to change and redistribute it.  
There is NO WARRANTY, to the extent permitted by law.  
See the file `COPYING` for copying conditions.  
