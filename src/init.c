/*------------ -------------- -------- --- ----- ---   --       -            -
 *  besssugo's initialization routines
 *
 *  Copyright (C) 2010--2013 jeremy theler
 *
 *  This file is part of besssugo.
 *
 *  besssugo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  besssugo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with wasora.  If not, see <http://www.gnu.org/licenses/>.
 *------------------- ------------  ----    --------  --     -       -         -
 */
#include "besssugo.h"

int plugin_init_before_parser(void) {
  
  // initialize SDL
  if (SDL_Init(SDL_INIT_VIDEO) == -1) {
    wasora_push_error_message("unable to initialize SDL: %s\n", SDL_GetError());
    return WASORA_RUNTIME_ERROR;
  }
  
  besssugo_init_colors();

  return WASORA_RUNTIME_OK;
}

// verificamos la consistencia del input
int plugin_init_after_parser(void) {
  return WASORA_RUNTIME_OK;
}

int plugin_init_before_run(void) {
  return WASORA_RUNTIME_OK;
}

int plugin_finalize(void) {
  
  canvas_t *canvas;
  
  for (canvas = besssugo.canvases; canvas != NULL; canvas = canvas->hh.next) {
    if (canvas->texture != NULL) {
      SDL_DestroyTexture(canvas->texture);
      canvas->texture = NULL;
    }
  }
    
  SDL_DestroyRenderer(besssugo.renderer);
  SDL_DestroyWindow(besssugo.window);
    
  SDL_QuitSubSystem(SDL_INIT_VIDEO);
  SDL_Quit();
  
  return WASORA_RUNTIME_OK;
}
