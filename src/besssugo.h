#include <SDL2/SDL.h>

#include "thirdparty/SDL_image.h"
#include "thirdparty/SDL2_gfxPrimitives.h"
#include "thirdparty/SDL2_rotozoom.h"

#include <wasora.h>

#define besssugo_eval_to_default(obj,expr,def) expr = (obj->expr.n_tokens!=0)?wasora_evaluate_expression(&obj->expr):(def);

// forward definitions
typedef struct canvas_t canvas_t;
typedef struct object_t object_t;
typedef struct dump_t dump_t;

typedef struct color_t color_t;

struct color_t {
  vector_t *vector;
  expr_t scalar[3];
  expr_t alpha;
};

struct canvas_t {
  int initialized;
  char *name;

  SDL_Texture *texture;
  
  Uint32 flags;
  int persistent;
  int opaque;
  int hardware;

  expr_t *coord;      // le hacemos un malloc(5) en parser
  double x0, y0, dx, dy, dr;
  
/*  
  enum {
    coords_natural,
    coords_graphical,
    coords_physical
  } coords;
*/
  expr_t top;
  expr_t left;
  expr_t xcenter;
  expr_t ycenter;

  expr_t width;
  expr_t height;

  color_t color;

  SDL_Rect rect;

  UT_hash_handle hh;
};

struct object_t {
  int initialized;
  char *name;
  
  enum {
    type_point,
    type_segment,
    type_rectangle,
    type_circle,
    type_image,
    // TODO: polygons
  } type;
  
  canvas_t *canvas;

  expr_t expr_n;
  int n;
  
  file_t *file;
  char *filepath;
  
  expr_t top;
  expr_t left;

  expr_t x[2];
  expr_t y[2];

  expr_t radius;

  expr_t angle_deg;
  expr_t angle_rad;

  color_t color;

  expr_t stroke_width;

  int stretch;
  int no_aa;
  int reload;
  int empty;
  
  SDL_Surface *surface;
  SDL_Texture *texture;
  SDL_Rect rect;

  object_t *next;  
};


struct dump_t {
  file_t *file;
  SDL_RWops *sdl_file;
  SDL_Surface *surface;
  SDL_Surface *info;
  void *pixels;
  
  dump_t *next;
};

struct {
  Uint32 windowflags;
  Uint32 rendererflags;
  
  char *caption;
  
  SDL_Window *window;
  SDL_Renderer *renderer;

  canvas_t *main_canvas;

  canvas_t *canvases;
  object_t *objects;
  dump_t *dumps;
  
} besssugo;

extern SDL_Surface *besssugo_load_image(file_t *, char *);

int besssugo_initialize_window(void);

extern int besssugo_init_colors(void);
extern int besssugo_evaluate_color(color_t *, Uint8 *, Uint8 *, Uint8 *, Uint8 *);
extern int besssugo_parser_color(color_t *);

extern signed int besssugo_evaluate_x(expr_t *, canvas_t *, double);
extern signed int besssugo_evaluate_y(expr_t *, canvas_t *, double);
extern signed int besssugo_evaluate_r(expr_t *, canvas_t *, double);

extern int besssugo_set_viewport_target(canvas_t *);
extern int besssugo_reset_viewport_target(canvas_t *c);

extern int besssugo_instruction_window(void *);
extern int besssugo_instruction_canvas(void *);
extern int besssugo_instruction_segment(void *);
extern int besssugo_instruction_point(void *);
extern int besssugo_instruction_rectangle(void *);
extern int besssugo_instruction_circle(void *);
extern int besssugo_instruction_image(void *);

extern int besssugo_instruction_draw(void *);
extern int besssugo_instruction_dump(void *);

extern int besssugo_circle_init(object_t *);