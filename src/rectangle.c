#include "besssugo.h"


int besssugo_instruction_rectangle(void *arg) {


  Sint16 x1, y1, x2, y2;
  Uint8 r, g, b, a;
  int i;
  object_t *rectangle = (object_t *)arg;

  if (!rectangle->initialized) {
    wasora_call(besssugo_rectangle_init(rectangle));
  }

  wasora_call(besssugo_set_viewport_target(rectangle->canvas));
  for (i = 1; i <= rectangle->n; i++) {
    wasora_var(wasora_special_var(i)) = (double)i;

    x1 = besssugo_evaluate_x(&rectangle->x[0], rectangle->canvas, -0.5);
    y1 = besssugo_evaluate_y(&rectangle->y[0], rectangle->canvas, -0.5);
    x2 = besssugo_evaluate_x(&rectangle->x[1], rectangle->canvas, 0.5);
    y2 = besssugo_evaluate_y(&rectangle->y[1], rectangle->canvas, 0.5);

    besssugo_evaluate_color(&rectangle->color, &r, &g, &b, &a);

    if (rectangle->empty) {
      rectangleRGBA(besssugo.renderer, x1, y1, x2, y2, r, g, b, a);
    } else {
      boxRGBA(besssugo.renderer, x1, y1, x2, y2, r, g, b, a);
    }
  }

  wasora_call(besssugo_reset_viewport_target(rectangle->canvas));
  
  return WASORA_RUNTIME_OK;

}

int besssugo_rectangle_init(object_t *rectangle) {
  if (rectangle->expr_n.n_tokens != 0) {
    rectangle->n = (int)(wasora_evaluate_expression(&rectangle->expr_n));
  }
  
  return WASORA_RUNTIME_OK;
}
