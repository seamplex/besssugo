#include "besssugo.h"


int besssugo_instruction_segment(void *arg) {
  

  Sint16 x1, y1, x2, y2;
  Uint8 r, g, b, a;
  Uint8 width;
  int i;
  object_t *segment = (object_t *)arg;  

  if (!segment->initialized) {
    wasora_call(besssugo_segment_init(segment));
  }
  
  wasora_call(besssugo_set_viewport_target(segment->canvas));
  for (i = 1; i <= segment->n; i++) {
    wasora_var(wasora_special_var(i)) = (double)i;

    x1 = besssugo_evaluate_x(&segment->x[0], segment->canvas, -0.5);
    y1 = besssugo_evaluate_y(&segment->y[0], segment->canvas, -0.5);
    x2 = besssugo_evaluate_x(&segment->x[1], segment->canvas, 0.5);
    y2 = besssugo_evaluate_y(&segment->y[1], segment->canvas, 0.5);
    width = besssugo_evaluate_r(&segment->stroke_width, segment->canvas, 0);

    besssugo_evaluate_color(&segment->color, &r, &g, &b, &a);

    if (width <= 1) {
      if (segment->no_aa) {
        lineRGBA(besssugo.renderer, x1, y1, x2, y2, r, g, b, a);
      } else {
        aalineRGBA(besssugo.renderer, x1, y1, x2, y2, r, g, b, a);
      }
    } else {
      thickLineRGBA(besssugo.renderer, x1, y1, x2, y2, width, r, g, b, a);
    }
  }

  wasora_call(besssugo_reset_viewport_target(segment->canvas));

  return WASORA_RUNTIME_OK;

}

int besssugo_segment_init(object_t *segment) {
  if (segment->expr_n.n_tokens != 0) {
    segment->n = (int)(wasora_evaluate_expression(&segment->expr_n));
  }
  
  return WASORA_RUNTIME_OK;
}
