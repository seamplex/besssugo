#include "besssugo.h"


int besssugo_instruction_circle(void *arg) {
  
  Sint16 x, y, radius;
  Uint8 r, g, b, a;
  int i;
  object_t *circle = (object_t *)arg;
  
  if (!circle->initialized) {
    wasora_call(besssugo_circle_init(circle));
  }

  wasora_call(besssugo_set_viewport_target(circle->canvas));
  for (i = 1; i <= circle->n; i++) {
    wasora_var(wasora_special_var(i)) = (double)i;
    
    x = besssugo_evaluate_x(&circle->x[0], circle->canvas, 0);
    y = besssugo_evaluate_y(&circle->y[0], circle->canvas, 0);
    radius = besssugo_evaluate_r(&circle->radius, circle->canvas, 0.5);

    besssugo_evaluate_color(&circle->color, &r, &g, &b, &a);

    if (radius > 0) {
      if (circle->empty == 0) {
        filledCircleRGBA(besssugo.renderer, x, y, radius, r, g, b, a);
      }

      if (circle->no_aa) {
        // este no hace falta si es filled no_aa
        circleRGBA(besssugo.renderer, x, y, radius, r, g, b, a);
      } else {
        aacircleRGBA(besssugo.renderer, x, y, radius, r, g, b, a);
      }
    }

  }
  wasora_call(besssugo_reset_viewport_target(circle->canvas));
    
  
  return WASORA_RUNTIME_OK;
  
}

int besssugo_circle_init(object_t *circle) {
  if (circle->expr_n.n_tokens != 0) {
    circle->n = (int)(wasora_evaluate_expression(&circle->expr_n));
  }
  
  return WASORA_RUNTIME_OK;
}
