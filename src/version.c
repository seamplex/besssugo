/*------------ -------------- -------- --- ----- ---   --       -            -
 *  besssugo's version banner
 *
 *  Copyright (C) 2010--2016 jeremy theler
 *
 *  This file is part of besssugo.
 *
 *  besssugo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  besssugo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with wasora.  If not, see <http://www.gnu.org/licenses/>.
 *------------------- ------------  ----    --------  --     -       -         -
 */

#include <stdio.h>

#include "besssugo.h"
#include "version.h"

// global static so the compiler locates these strings in the text section
// so when the plugin_* functions return pointers to the strings there is
// no need to free them afterward
const char besssugoname[] = "besssugo";
const char besssugodescription[] = "scientific video generator";
char besssugoshortversion[128];
char besssugolongversion[2048];
const char besssugousage[] = "";

const char besssugocopyright[] = "\
 besssugo is copyright (c) 2012-2016 jeremy theler\n\
 licensed under GNU GPL version 3 or later.\n\
 besssugo is free software: you are free to change and redistribute it.\n\
 There is NO WARRANTY, to the extent permitted by law.";

const char besssugohmd5[] = PLUGIN_HEADERMD5;


const char *plugin_name(void) {
  return besssugoname;
}

const char *plugin_longversion(void) {
  
#ifdef PLUGIN_VCS_BRANCH
  sprintf(besssugolongversion, "\n\
 last commit on %s\n\
 compiled on %s by %s@%s (%s)\n\
 with %s using %s\n",
   PLUGIN_VCS_DATE,
   COMPILATION_DATE,
   COMPILATION_USERNAME,
   COMPILATION_HOSTNAME,
   COMPILATION_ARCH,
   CCOMPILER_VERSION,
   CCOMPILER_FLAGS);
#endif

  return besssugolongversion;
}

const char *plugin_wasorahmd5(void) {
  return besssugohmd5;
}
const char *plugin_copyright(void) {
  return besssugocopyright;
}


const char *plugin_version(void) {
#ifdef PLUGIN_VCS_BRANCH
  sprintf(besssugoshortversion, "%s%s %s", PLUGIN_VCS_VERSION,
                                           (PLUGIN_VCS_CLEAN==0)?"":"+Δ",
                                           strcmp(PLUGIN_VCS_BRANCH, "master")?PLUGIN_VCS_BRANCH:"");
#else
  sprintf(besssugoshortversion, "%s", PACKAGE_VERSION);
#endif
  
  return besssugoshortversion;
}

const char *plugin_description(void) {
  return besssugodescription;
}

const char *plugin_usage(void) {
  return besssugousage;
}
