#include <stdio.h>
#include <string.h>

#include "besssugo.h"


int plugin_parse_line(char *line) {

  char *token;
  expr_t *expr = NULL;
  
  if ((token = wasora_get_next_token(line)) != NULL) {

    // ----- WINDOW ---------------------------------------------------
    if (strcasecmp(token, "WINDOW") == 0 || strcasecmp(token, "CANVAS") == 0) {

      int window = 0;
      int gotit = 0;
      canvas_t *canvas;
      
      if (strcasecmp(token, "WINDOW") == 0) {
        window = 1;
        // creamos el canvas de la ventana
        besssugo.main_canvas = calloc(1, sizeof(canvas_t));
        besssugo.main_canvas->name = strdup("main");
        HASH_ADD_KEYPTR(hh, besssugo.canvases, besssugo.main_canvas->name, strlen(besssugo.main_canvas->name), besssugo.main_canvas);
        canvas = besssugo.main_canvas;
        // la ventana principal es opaca por default (los canvas no)
        canvas->opaque = 1;
      } else {
        canvas = calloc(1, sizeof(canvas_t));
        if ((token = wasora_get_next_token(NULL)) == NULL) {
          wasora_push_error_message("expected canvas name");
          return WASORA_PARSER_ERROR;
        }
        canvas->name = strdup(token);
      }

     
      while ((token = wasora_get_next_token(NULL)) != NULL) {
        gotit = 0;
        if (window) {
          // ---- CAPTION --------------------------------------------------
          if (strcasecmp(token, "CAPTION") == 0) {
            wasora_call(wasora_parser_string(&besssugo.caption));
            gotit = 1;
          
          // ---- SDL_WINDOW  ---------------------------------------------
          } else if (strcasecmp(token, "FULLSCREEN") == 0) {
            besssugo.windowflags |= SDL_WINDOW_FULLSCREEN;
            gotit = 1;
          } else if (strcasecmp(token, "FULLSCREEN_DESKTOP") == 0) {
            besssugo.windowflags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
            gotit = 1;
          } else if (strcasecmp(token, "HIDDEN") == 0) {
            besssugo.windowflags |= SDL_WINDOW_HIDDEN;
            gotit = 1;
          } else if (strcasecmp(token, "BORDERLESS") == 0) {
            besssugo.windowflags |= SDL_WINDOW_BORDERLESS;
            gotit = 1;
          } else if (strcasecmp(token, "MINIMIZED") == 0) {
            besssugo.windowflags |= SDL_WINDOW_MINIMIZED;
            gotit = 1;
          } else if (strcasecmp(token, "INPUT_GRABBED") == 0) {
            besssugo.windowflags |= SDL_WINDOW_INPUT_GRABBED;
            gotit = 1;
#ifdef SDL_WINDOW_ALLOW_HIGHDPI            
          } else if (strcasecmp(token, "ALLOW_HIGHDPI") == 0) {
            besssugo.windowflags |= SDL_WINDOW_ALLOW_HIGHDPI;
            gotit = 1;
#endif
            
          // ---- SDL_RENDERER  ---------------------------------------------
          } else if (strcasecmp(token, "SOFTWARE") == 0) {
            besssugo.rendererflags |= SDL_RENDERER_SOFTWARE;
            gotit = 1;
          } else if (strcasecmp(token, "ACCELERATED") == 0) {
            besssugo.rendererflags |= SDL_RENDERER_ACCELERATED;
            gotit = 1;
          } else if (strcasecmp(token, "PRESENTVSYNC") == 0) {
            besssugo.rendererflags |= SDL_RENDERER_PRESENTVSYNC;
            gotit = 1;
          }
        }
        if (strcasecmp(token, "COORDINATES") == 0 || strcasecmp(token, "COORDS") == 0) {

          canvas->coord = calloc(5, sizeof(expr_t));
          wasora_call(wasora_parser_expressions(&canvas->coord, 5));

        } else if (strcasecmp(token, "PERSISTENT") == 0) {
          canvas->persistent = 1;
        } else if (strcasecmp(token, "UNPERSISTENT") == 0) {
          canvas->persistent = 0;
        } else if (strcasecmp(token, "OPAQUE") == 0) {
          canvas->opaque = 1;
        } else if (strcasecmp(token, "TRANSPARENT") == 0) {
          canvas->opaque = 0;
        } else if (strcasecmp(token, "HARDWARE") == 0) {
          canvas->hardware = 1;
        } else if (strcasecmp(token, "TOP") == 0) {
          wasora_call(wasora_parser_expression(expr = &canvas->top));
        } else if (strcasecmp(token, "LEFT") == 0) {
          wasora_call(wasora_parser_expression(expr = &canvas->left));
        } else if (strcasecmp(token, "XCENTER") == 0) {
          wasora_call(wasora_parser_expression(expr = &canvas->xcenter));
        } else if (strcasecmp(token, "YCENTER") == 0) {
          wasora_call(wasora_parser_expression(expr = &canvas->ycenter));
        } else if (strcasecmp(token, "WIDTH") == 0) {
          wasora_call(wasora_parser_expression(expr = &canvas->width));
        } else if (strcasecmp(token, "HEIGHT") == 0) {
          wasora_call(wasora_parser_expression(expr = &canvas->height));
        } else if (strcasecmp(token, "COLOR") == 0) {
          wasora_call(besssugo_parser_color(&canvas->color));
        } else if (strcasecmp(token, "ALPHA") == 0) {
          wasora_call(wasora_parser_expression(&canvas->color.alpha));
        } else if (gotit == 0) {
          wasora_push_error_message("undefined keyword '%s'", token);
          return WASORA_PARSER_ERROR;
        }
      }

      if (window) {
        
        if (besssugo.main_canvas->width.n_tokens == 0) {
          wasora_push_error_message("expected window width");
          return WASORA_PARSER_ERROR;
        }
        if (besssugo.main_canvas->height.n_tokens == 0) {
          wasora_push_error_message("expected window height");
          return WASORA_PARSER_ERROR;
        }
        
        wasora_define_instruction(besssugo_instruction_window, NULL);
        
      } else {

        // si no es la ventana lo agregamos al hash list
        HASH_ADD_KEYPTR(hh, besssugo.canvases, canvas->name, strlen(canvas->name), canvas);
        wasora_define_instruction(besssugo_instruction_canvas, canvas);
      }
      
      return WASORA_PARSER_OK;

/* --------- BESSSUGO_DRAW --------------------------------------*/
    } else if (strcasecmp(token, "BESSSUGO_DRAW") == 0) {
      
      wasora_define_instruction(besssugo_instruction_draw, NULL);
      return WASORA_PARSER_OK;

/* --------- BESSSUGO_DUMP --------------------------------------*/
    } else if (strcasecmp(token, "BESSSUGO_DUMP") == 0) {

      dump_t *dump = calloc(1, sizeof(dump_t));
      
      while ((token = wasora_get_next_token(NULL)) != NULL) {
        expr = NULL;
        if (strcasecmp(token, "FILE") == 0) {
          if (wasora_parser_file(&dump->file) != WASORA_PARSER_OK) {
            return WASORA_PARSER_ERROR;
          }
        } else {
          wasora_push_error_message("undefined keyword '%s'", token);
          return WASORA_PARSER_ERROR;
        }
      }

      LL_APPEND(besssugo.dumps, dump);
      wasora_define_instruction(besssugo_instruction_dump, dump);
      return WASORA_PARSER_OK;
      

/* --------- OBJECT ---------------------------------------------*/
    } else if (strcasecmp(token, "POINT") == 0 || 
               strcasecmp(token, "SEGMENT") == 0 || strcasecmp(token, "LINE") == 0 ||
               strcasecmp(token, "RECTANGLE") == 0 || strcasecmp(token, "BOX") == 0 ||
               strcasecmp(token, "CIRCLE") == 0 ||
               strcasecmp(token, "IMAGE") == 0) {

      object_t *object = calloc(1, sizeof(object_t));
      object->canvas = besssugo.main_canvas;
      object->n = 1;
      
      if (strcasecmp(token, "POINT") == 0) {
        object->type = type_point;
      } else if (strcasecmp(token, "SEGMENT") == 0 || strcasecmp(token, "LINE") == 0) {
        object->type = type_segment;
      } else if (strcasecmp(token, "RECTANGLE") == 0 || strcasecmp(token, "BOX") == 0) {
        object->type = type_rectangle;
      } else if (strcasecmp(token, "CIRCLE") == 0) {
        object->type = type_circle;
      } else if (strcasecmp(token, "IMAGE") == 0) {
        object->type = type_image;
      }

      while ((token = wasora_get_next_token(NULL)) != NULL) {

        if (strcasecmp(token, "ARRAY") == 0) {
          wasora_call(wasora_parser_expression(&object->expr_n));
          
        } else if (strcasecmp(token, "CANVAS") == 0) {
          char *canvas_name;
          
          wasora_call(wasora_parser_string(&canvas_name));
          HASH_FIND_STR(besssugo.canvases, canvas_name, object->canvas);
          if (object->canvas == NULL) {
            wasora_push_error_message("undefined canvas '%s'", canvas_name);
            return WASORA_PARSER_ERROR;
          }

        } else if (strcasecmp(token, "FILE_PATH") == 0) {
          if (wasora_parser_string(&object->filepath) != WASORA_PARSER_OK) {
            return WASORA_PARSER_ERROR;
          }

        } else if (strcasecmp(token, "FILE") == 0) {
          if (wasora_parser_file(&object->file) != WASORA_PARSER_OK) {
            return WASORA_PARSER_ERROR;
          }

        } else if (strcasecmp(token, "X") == 0 || strcasecmp(token, "X1") == 0 || strcasecmp(token, "XCENTER") == 0) {
          wasora_call(wasora_parser_expression(&object->x[0]));
        } else if (strcasecmp(token, "Y") == 0 || strcasecmp(token, "Y1") == 0 || strcasecmp(token, "YCENTER") == 0) {
          wasora_call(wasora_parser_expression(&object->y[0]));
        } else if (strcasecmp(token, "X2") == 0) {
          wasora_call(wasora_parser_expression(&object->x[1]));
        } else if (strcasecmp(token, "Y2") == 0) {
          wasora_call(wasora_parser_expression(&object->y[1]));
          
        } else if (strcasecmp(token, "R") == 0 || strcasecmp(token, "RAD") == 0 || strcasecmp(token, "RADIUS") == 0 ) {
          wasora_call(wasora_parser_expression(&object->radius));

        } else if (strcasecmp(token, "TOP") == 0) {
          wasora_call(wasora_parser_expression(&object->top));
        } else if (strcasecmp(token, "LEFT") == 0) {
          wasora_call(wasora_parser_expression(&object->left));
        } else if (strcasecmp(token, "ANGLE_DEG") == 0) {
          wasora_call(wasora_parser_expression(&object->angle_deg));
        } else if (strcasecmp(token, "ANGLE_RAD") == 0) {
          wasora_call(wasora_parser_expression(&object->angle_rad));
          
        } else if (strcasecmp(token, "STROKE") == 0 || strcasecmp(token, "STROKE_WIDTH") == 0) {
          wasora_call(wasora_parser_expression(&object->stroke_width));
          
         
        } else if (strcasecmp(token, "COLOR") == 0) {
          wasora_call(besssugo_parser_color(&object->color));
          
        } else if (strcasecmp(token, "ALPHA") == 0) {
          wasora_call(wasora_parser_expression(&object->color.alpha));

        } else if (strcasecmp(token, "NO_AA") == 0) {
          object->no_aa = 1;
        } else if (strcasecmp(token, "AA") == 0) {
          object->no_aa = 0;
          
        } else if (strcasecmp(token, "EMPTY") == 0) {
          object->empty = 1;
        } else if (strcasecmp(token, "FILLED") == 0) {
          object->empty = 0;
          
        } else if (strcasecmp(token, "NO_RELOAD") == 0) {
          object->reload = 0;
        } else if (strcasecmp(token, "RELOAD") == 0) {
          object->reload = 1;

        } else if (strcasecmp(token, "STRETCH") == 0) {
          object->stretch = 1;
          
        } else {
          wasora_push_error_message("undefined keyword '%s'", token);
          return WASORA_PARSER_ERROR;
        }

      }


      LL_APPEND(besssugo.objects, object);
      
      switch (object->type) {
        case type_point:
          wasora_define_instruction(besssugo_instruction_point, object);
        break;
        case type_segment:
          wasora_define_instruction(besssugo_instruction_segment, object);
        break;
        case type_rectangle:
          wasora_define_instruction(besssugo_instruction_rectangle, object);
        break;
        case type_circle:
          wasora_define_instruction(besssugo_instruction_circle, object);
        break;
        case type_image:
          wasora_define_instruction(besssugo_instruction_image, object);
        break;
      }
      
      return WASORA_PARSER_OK;
    }
  }
    
  return WASORA_PARSER_UNHANDLED;

}



int besssugo_parser_color(color_t *color) {
  
  char *token;

  // no sabemos que es el primero  
  if ((token = wasora_get_next_token(NULL)) == NULL) {
    wasora_push_error_message("expected expression");
    return WASORA_PARSER_ERROR;
  }

  // intentamos con un vector  
  if ((color->vector = wasora_get_vector_ptr(token)) == NULL) {
    
    // y si no son tres expresiones (la primera ya la leimos)
    if (wasora_parse_expression(token, &color->scalar[0]) != WASORA_PARSER_OK) {
      return WASORA_PARSER_ERROR;
    }
    
    if ((token = wasora_get_next_token(NULL)) == NULL) {
      wasora_push_error_message("expected expression for green color");
      return WASORA_PARSER_ERROR;
    }
    if (wasora_parse_expression(token, &color->scalar[1]) != WASORA_PARSER_OK) {
      return WASORA_PARSER_ERROR;
    }

    if ((token = wasora_get_next_token(NULL)) == NULL) {
      wasora_push_error_message("expected expression for blue color");
      return WASORA_PARSER_ERROR;
    }
    if (wasora_parse_expression(token, &color->scalar[2]) != WASORA_PARSER_OK) {
      return WASORA_PARSER_ERROR;
    }
  } else {
    if (color->vector->size != 3) {
      wasora_push_error_message("color vector '%s' has to have size three");
      return WASORA_PARSER_ERROR;
    }
  }
  
  return WASORA_PARSER_OK;
}