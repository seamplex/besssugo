#include "besssugo.h"


int besssugo_instruction_dump(void *arg) {
  
  dump_t *dump = (dump_t *)arg;
  
  if ((dump->info = SDL_GetWindowSurface(besssugo.window)) == NULL) {
    wasora_push_error_message("unable to get surface from window");
    return WASORA_RUNTIME_ERROR;
  }
  
  dump->pixels = malloc(dump->info->w * dump->info->h * dump->info->format->BytesPerPixel);  
  SDL_RenderReadPixels(besssugo.renderer, &dump->info->clip_rect, dump->info->format->format, dump->pixels, dump->info->w * dump->info->format->BytesPerPixel);
  dump->surface = SDL_CreateRGBSurfaceFrom(dump->pixels, dump->info->w, dump->info->h, dump->info->format->BitsPerPixel, dump->info->w * dump->info->format->BytesPerPixel, dump->info->format->Rmask, dump->info->format->Gmask, dump->info->format->Bmask, dump->info->format->Amask);  

  if (dump->file->pointer == NULL) {
    wasora_call(wasora_instruction_open_file(dump->file));
  }
  if ((dump->sdl_file = SDL_RWFromFP(dump->file->pointer, SDL_FALSE)) == NULL) {
    wasora_push_error_message("error converting from file to SDL_RW: %s", SDL_GetError());
    return WASORA_RUNTIME_ERROR;
  }
  
  if (IMG_SavePNG_RW(dump->surface, dump->sdl_file, SDL_FALSE) != 0) {
    wasora_push_error_message("SDL error '%s' when exporting to PNG", SDL_GetError());
    return WASORA_RUNTIME_ERROR;
  }

  wasora_instruction_close_file(dump->file);
  
  SDL_FreeSurface(dump->surface);
  dump->surface = NULL;  

  free(dump->pixels);

  SDL_FreeSurface(dump->info);
  dump->info = NULL;


  return WASORA_RUNTIME_OK;
}
