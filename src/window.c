#include "besssugo.h"

extern char besssugoshortversion[128];
extern char besssugoname[];

int besssugo_instruction_window(void *arg) {
  
  int width, height;
  
  if (besssugo.window == NULL) {
    width = wasora_evaluate_expression(&besssugo.main_canvas->width);
    height = wasora_evaluate_expression(&besssugo.main_canvas->height);

    if (besssugo.caption == NULL) {
      besssugo.caption = malloc(strlen(besssugoname) + strlen(besssugoshortversion) + 1);
      sprintf(besssugo.caption, "%s %s", besssugoname, besssugoshortversion);
    }

    // initialize the display
    if ((besssugo.window = SDL_CreateWindow(besssugo.caption, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, besssugo.windowflags)) == NULL) {
      wasora_push_error_message("unable to create a %dx%d window: %s\n", width, height, SDL_GetError());
      return WASORA_RUNTIME_ERROR;
    }
    if ((besssugo.renderer = SDL_CreateRenderer(besssugo.window, -1, besssugo.rendererflags)) == NULL) {
      wasora_push_error_message("unable to create window renderer: %s\n", SDL_GetError());
      return WASORA_RUNTIME_ERROR;
    }
    
    if (besssugo.main_canvas->color.vector == NULL &&
        besssugo.main_canvas->color.scalar[0].n_tokens == 0 &&
        besssugo.main_canvas->color.scalar[1].n_tokens == 0 &&
        besssugo.main_canvas->color.scalar[2].n_tokens == 0) {
      besssugo.main_canvas->color.vector = wasora_get_vector_ptr("white");
    }
    
  }
  
  wasora_call(besssugo_instruction_canvas(besssugo.main_canvas));
  
  return WASORA_RUNTIME_OK;
}