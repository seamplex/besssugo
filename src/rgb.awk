# this script is called from autogen.sh and filters
# rgb.txt (which is a copy of /etc/X11/rgb.txt) and
# generates a source file that initializes x11 color names
# as wasora vectors ready to be used from within besssugo
BEGIN {
 print "#include \"besssugo.h\""
 print ""
 print "int besssugo_init_colors(void) {"
 print ""
 print "  vector_t *vector;"

}
{
 # ignore colornames that contain spaces
 # and "tan" because it clashes with the trigonometric function
 if ($5 == "" && $4 != "tan") {
   printf("\n");
   printf("  vector = wasora_define_vector(\"%s\", 3, NULL, NULL);\n", tolower($4));
   printf("  wasora_vector_init(vector);\n");
   printf("  gsl_vector_set(wasora_value_ptr(vector), 0, %d.0/255.0);\n", $1);
   printf("  gsl_vector_set(wasora_value_ptr(vector), 1, %d.0/255.0);\n", $2);
   printf("  gsl_vector_set(wasora_value_ptr(vector), 2, %d.0/255.0);\n", $3);
 }
}
END {
 print ""
 print "  return WASORA_RUNTIME_OK;"
 print "}"
}
