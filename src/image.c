#include "besssugo.h"


int besssugo_instruction_image(void *arg) {
  
  int xcenter, ycenter;
  int left, top;
  double angle = 0;
  object_t *image = (object_t *)arg;
 
  SDL_Surface *rotated_img = NULL;

  wasora_call(besssugo_set_viewport_target(image->canvas));
  
  if (image->surface == NULL) {
    if ((image->surface = besssugo_load_image(image->file, image->filepath)) == NULL) {
      return WASORA_PARSER_ERROR;
    }
  }

  if (image->angle_deg.n_tokens != 0) {
    angle = wasora_evaluate_expression(&image->angle_deg);
  }
  if (image->angle_rad.n_tokens != 0) {
    angle = wasora_evaluate_expression(&image->angle_rad)*180.0/M_PI;
  }
  if (angle != 0) {
    rotated_img = rotozoomSurface(image->surface, angle, 1, !image->no_aa);
    // TODO: llamar a la rutina que te lo calcula (API)
    image->rect.w = rotated_img->clip_rect.w;
    image->rect.h = rotated_img->clip_rect.h;
    image->texture = SDL_CreateTextureFromSurface(besssugo.renderer, rotated_img);
  } else {
    // TODO: usar API
    image->rect.w = image->surface->clip_rect.w;
    image->rect.h = image->surface->clip_rect.h;
    image->texture = SDL_CreateTextureFromSurface(besssugo.renderer, image->surface);
  }
  
  if (image->top.n_tokens != 0 || image->left.n_tokens != 0) {
    left = besssugo_evaluate_x(&image->left, image->canvas, 0);
    top  = besssugo_evaluate_y(&image->top, image->canvas, 0);
  } else {
    xcenter = besssugo_evaluate_x(&image->x[0], image->canvas, 0);
    ycenter = besssugo_evaluate_y(&image->y[0], image->canvas, 0);
    left = xcenter - 0.5*image->rect.w;
    top  = ycenter - 0.5*image->rect.h;
  }
  image->rect.x = left;
  image->rect.y = top;
  
  if (image->stretch) {
    SDL_RenderCopy(besssugo.renderer, image->texture, NULL, &image->canvas->rect);
  } else {
    SDL_RenderCopy(besssugo.renderer, image->texture, NULL, &image->rect);
  }

  if (rotated_img != NULL) {
    SDL_FreeSurface(rotated_img);
  }

  if (image->reload) {
    SDL_FreeSurface(image->surface);
    image->surface = NULL;
  }
  SDL_DestroyTexture(image->texture);
  image->texture = NULL;

  wasora_call(besssugo_reset_viewport_target(image->canvas));
  
  return WASORA_RUNTIME_OK;
  
}


SDL_Surface *besssugo_load_image(file_t *file, char *filepath) {

  SDL_Surface *surface;
  SDL_RWops *rw;
  FILE *handle;
  
  if (file != NULL) {
    if (wasora_instruction_open_file(file) != WASORA_RUNTIME_OK) {
      return NULL;
    }
    handle = file->pointer;
  } else if (filepath != NULL) {
    if ((handle = wasora_fopen(filepath, "r")) == NULL) {
      wasora_push_error_message("error opening file '%s': %s", filepath, strerror(errno));
      return NULL;
    }
  } else {
    return NULL;
  }
    
  if ((rw = SDL_RWFromFP(handle, SDL_TRUE)) == NULL) {
    wasora_push_error_message("error converting file '%s' to RW: %s", (file!=NULL)?file->path:filepath, SDL_GetError());
    return NULL;
  }
  if ((surface = IMG_Load_RW(rw, SDL_TRUE)) == NULL) {
    wasora_push_error_message("error opening image '%s': %s", (file!=NULL)?file->path:filepath, SDL_GetError());
    return NULL;
  }
    
  return surface;
  
}
