#include "besssugo.h"

int besssugo_instruction_point(void *arg) {

  Sint16 x, y;
  Uint8 r, g, b, a;
  object_t *point = (object_t *)arg;

  wasora_call(besssugo_set_viewport_target(point->canvas));
  
  x = besssugo_evaluate_x(&point->x[0], point->canvas, 0);
  y = besssugo_evaluate_y(&point->y[0], point->canvas, 0);

  besssugo_evaluate_color(&point->color, &r, &g, &b, &a);

  pixelRGBA(besssugo.renderer, x, y, r, g, b, a);

  wasora_call(besssugo_reset_viewport_target(point->canvas));
  
  return WASORA_RUNTIME_OK;

}

int besssugo_point_init(object_t *point) {
  if (point->expr_n.n_tokens != 0) {
    point->n = (int)(wasora_evaluate_expression(&point->expr_n));
  }
  
  return WASORA_RUNTIME_OK;
}
