#include "besssugo.h"

int besssugo_instruction_canvas(void *arg) {
  
  int width, height;
  int top, left;
  Uint8 r, g, b, a;
  canvas_t *canvas = (canvas_t *)arg;

  if (canvas == besssugo.main_canvas) {
    top = 0;
    left = 0;
    SDL_GetWindowSize(besssugo.window, &width, &height);
  } else {
    left = besssugo_evaluate_x(&canvas->left, besssugo.main_canvas, -1.0);
    top = besssugo_evaluate_y(&canvas->top, besssugo.main_canvas, 1.0);
    width = besssugo_evaluate_r(&canvas->width, besssugo.main_canvas, 2.0);
    height = besssugo_evaluate_r(&canvas->height, besssugo.main_canvas, 2.0);
  }

  canvas->rect.x = left;
  canvas->rect.y = top;
  canvas->rect.w = width;
  canvas->rect.h = height;

  besssugo_evaluate_color(&canvas->color, &r, &g, &b, &a);

  // aca tenemos que sobre-escribir el default que evaluo evaluate_color
  if (canvas->color.alpha.n_tokens == 0) {
    a = (canvas->opaque) ? SDL_ALPHA_OPAQUE : SDL_ALPHA_TRANSPARENT;
  }

  if (!canvas->initialized) {
    
    if (canvas->hardware == 0) {
      if ((canvas->texture = SDL_CreateTexture(besssugo.renderer, SDL_PIXELFORMAT_UNKNOWN, SDL_TEXTUREACCESS_TARGET, width, height)) == NULL) {
        wasora_push_error_message("unable to create texture for canvas '%s': %s\n", canvas->name, SDL_GetError());
        return WASORA_RUNTIME_ERROR;
      }
      SDL_SetTextureBlendMode(canvas->texture, SDL_BLENDMODE_BLEND);
    } else {
      SDL_SetRenderDrawBlendMode(besssugo.renderer, SDL_BLENDMODE_BLEND);
    }
    
    
    // coordenadas
    if (canvas->coord != NULL) {
      canvas->x0 = wasora_evaluate_expression(&canvas->coord[0]);
      canvas->y0 = wasora_evaluate_expression(&canvas->coord[1]);
      canvas->dx = wasora_evaluate_expression(&canvas->coord[2]);
      canvas->dy = wasora_evaluate_expression(&canvas->coord[3]);
      canvas->dr = wasora_evaluate_expression(&canvas->coord[4]);
    } else {
      // naturales manda x
      canvas->x0 = canvas->rect.w/2;
      canvas->y0 = canvas->rect.h/2;
      canvas->dx = canvas->rect.w/2;
      canvas->dy = -canvas->rect.w/2;
      canvas->dr = canvas->rect.w/2;
    }
  }


  if (canvas->persistent == 0 || !canvas->initialized) {
    wasora_call(besssugo_set_viewport_target(canvas));
    SDL_SetRenderDrawColor(besssugo.renderer, r, g, b, a);
    SDL_RenderClear(besssugo.renderer);
    wasora_call(besssugo_reset_viewport_target(canvas));
  }
    
  
  canvas->initialized = 1;

  return WASORA_RUNTIME_OK;
}
