#include <math.h>

#include "besssugo.h"

int besssugo_instruction_draw(void *arg) {

  canvas_t *canvas;
  
  // seteamos la ventana como renderer
  SDL_SetRenderTarget(besssugo.renderer, NULL);

  // barremos los canvas y los copiamos (solo los que son persistentes)
  for (canvas = besssugo.canvases; canvas != NULL; canvas = canvas->hh.next) {
//    if (canvas->persistent) {
      SDL_RenderCopy(besssugo.renderer, canvas->texture, NULL, &canvas->rect);
//    }
  }
  
  // dibujamos
  SDL_RenderPresent(besssugo.renderer);
  
  return WASORA_RUNTIME_OK;
}

int besssugo_evaluate_color(color_t *color, Uint8 *r, Uint8 *g, Uint8 *b, Uint8 *a) {
  
  if (color->vector != NULL) {
    *r = (Uint8)(255*fabs(gsl_vector_get(wasora_value_ptr(color->vector), 0)));
    *g = (Uint8)(255*fabs(gsl_vector_get(wasora_value_ptr(color->vector), 1)));
    *b = (Uint8)(255*fabs(gsl_vector_get(wasora_value_ptr(color->vector), 2)));
  } else {
    *r = (Uint8)(255*fabs(wasora_evaluate_expression(&color->scalar[0])));
    *g = (Uint8)(255*fabs(wasora_evaluate_expression(&color->scalar[1])));
    *b = (Uint8)(255*fabs(wasora_evaluate_expression(&color->scalar[2])));
  }
  
   *a = (color->alpha.n_tokens!=0)?(Uint8)(255*fabs(wasora_evaluate_expression(&color->alpha))):(SDL_ALPHA_OPAQUE);
   
   return WASORA_RUNTIME_OK;
  
}

signed int besssugo_evaluate_x(expr_t *expr, canvas_t *canvas, double def) {
  return (signed int)(rint(canvas->x0 + canvas->dx * ((expr->n_tokens != 0) ? wasora_evaluate_expression(expr) : def)));
}

signed int besssugo_evaluate_y(expr_t *expr, canvas_t *canvas, double def) {
  return (signed int)(rint(canvas->y0 + canvas->dy * ((expr->n_tokens != 0) ? wasora_evaluate_expression(expr) : def)));
}

signed int besssugo_evaluate_r(expr_t *expr, canvas_t *canvas, double def) {
  return (signed int)(rint(canvas->dr * ((expr->n_tokens != 0) ? wasora_evaluate_expression(expr) : def)));
}

int besssugo_set_viewport_target(canvas_t *canvas) {

  if (canvas->hardware == 0) {
    if (SDL_SetRenderTarget(besssugo.renderer, canvas->texture) != 0) {
      wasora_push_error_message("unable to set texture target for canvas '%s': %s\n", canvas->name, SDL_GetError());
      return WASORA_RUNTIME_ERROR;
    }
  } else {
    if (SDL_RenderSetViewport(besssugo.renderer, &canvas->rect) != 0) {
      wasora_push_error_message("unable to set viewport for canvas '%s': %s\n", canvas->name, SDL_GetError());
      return WASORA_RUNTIME_ERROR;
    }
  }
  
  return WASORA_RUNTIME_OK;
  
}

int besssugo_reset_viewport_target(canvas_t *canvas) {

  if (canvas->hardware == 0) {
    if (SDL_SetRenderTarget(besssugo.renderer, NULL) != 0) {
      wasora_push_error_message("unable to reset viewport for canvas '%s': %s\n", canvas->name, SDL_GetError());
      return WASORA_RUNTIME_ERROR;
    }
  } else {
    if (SDL_RenderSetViewport(besssugo.renderer, NULL) != 0) {
      wasora_push_error_message("unable to reset viewport for canvas '%s': %s\n", canvas->name, SDL_GetError());
      return WASORA_RUNTIME_ERROR;
    }
  }
  
  return WASORA_RUNTIME_OK;
  
}