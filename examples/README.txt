ball.was
--------

 Execute 

  $ wasora -p besssugo ball.was

 and watch the yellow bouncing ball. It changes its speed every time!
 
 If wasora is started with -d (and was compiled with libreadline), it should start in debug mode and a step-by-step advancement can be performed by hitting enter after the prompt in the terminal. Try it!

  $ wasora -d -p besssugo ball.was

 Type "help" to get it.


video.was
---------

  Provided avconv is installed, execute

   $ wasora -p besssugo video 20

  and obtain a video called ball.avi, where 20 is the desired frames-per-second. Try different values of FPS!


objects.was
-----------

 $ wasora -p besssugo objects.was

  An illustration of the many graphical objects besssugo can handle.




