if [[ -x ./besssugo ]] && [[ ! -h ./besssugo ]]; then
 besssugobin=./besssugo
 testdir="examples/"
elif [ -x ../besssugo ]; then
 besssugobin="../besssugo"
 testdir="./"
elif [ ! -z "`which wasora`" ]; then
 if [ -x ./besssugo.so ]; then
  besssugobin="wasora -p besssugo.so"
  testdir="examples/"
 elif [ -x ../besssugo.so ]; then
  besssugobin="wasora -p ../besssugo.so"
  testdir=""
 fi
else
 echo "do not know how to run besssugo :("
 exit 1
fi

# checks if avconv is installed
function checkavconv {
 if [ -z "`which avconv`" ]; then
  echo "avconv is not installed, skipping test"
  exit 77
 fi
}

# calls mplayer
function callmplayer {
 if [ "x`which mplayer`" != "x" ]; then
  mplayer $1 
 fi
}
